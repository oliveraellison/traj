#! /usr/bin/python
import sys
import random
import math
import tempfile
import itertools as IT
import os


time_point = 0;     # value in 1st column
x_point = 0.01;     # value in 2nd column
y_point = 0.01;     # value in 3rd column
z_point = 0.01;     # value in 4th column
      
trajectories = 1002; # number of trajectories to generate
steps = 1002;       # number of steps per trajectory

file_num = 1;       # numbers the files

while file_num < trajectories:  # creates the desired number of trajectory files

    file_num += 1; # number iteratively appended to filenames of generated trajectories 

    def numberedFile(path, sep = ''): # increases the number for each new file name by 1 
        def name_sequence():
            count = IT.count()
            yield ''
            while True:
                yield '{s}{n:d}'.format(s = sep, n = next(count))
        orig = tempfile._name_sequence 
        with tempfile._once_lock:
            tempfile._name_sequence = name_sequence()
            path = os.path.normpath(path)
            dirname, basename = os.path.split(path)
            filename, ext = os.path.splitext(basename)
            fd, filename = tempfile.mkstemp(dir = dirname, prefix = filename, suffix = ext)
            tempfile._name_sequence = orig
        return filename

    f = open(numberedFile('trajectories/trajectory.dat'), "w") # filename will be trajectory1.dat ... trajectoryN.dat

    time_point = 0; # numbers each timestep in the trajectory

    n = 0; 



    while (n < steps):
        n+=1;
        time_point += 1;

        x_point = x_point + (random.randrange(0,100)-random.randrange(0,50))*.0000333;
        y_point = y_point + (random.randrange(0,100)-random.randrange(0,50))*.0000333;
        z_point = z_point + (random.randrange(0,100)-random.randrange(0,50))*.0000333;
 
        f.write(str(x_point) + ' ' + str(y_point) + ' ' + str(z_point) + '\n');


    print("Missile strike at: ", x_point, ", ", y_point, ", ", z_point); # prints the last point of each trajectory 

    f.close();      # closes each file before creating new file

    x_point = .01;  # resets values before starting new trajectory
    y_point = .01;
    z_point = .01;







