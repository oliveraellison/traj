#Script to generate a 3D figure

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np


style.use('fivethirtyeight')

trajectories = 7;

n = 0

while n < trajectories:
    N_str1 = str(n)

    # reads first 3D trajectory                      
    w, wi, er = np.loadtxt('trajectories/trajectory' + N_str1 + '.dat', delimiter=' ', usecols = (0, 1, 2), unpack=True )
    
    N_str2 = str(n + 1)

    # reads second 3D trajectory 
    x, y, z = np.loadtxt('trajectories/trajectory' + N_str2 + '.dat', delimiter=' ', usecols = (0, 1, 2), unpack=True )


    fig = plt.figure()
    ax = plt.axes(projection='3d')

    #Coordinate of the target point with a blue dot.
    x0 = [0,0]
    y0 = [0,0]
    z0 = [0,0]
    ax.plot(x0, y0 ,z0, 'bo')

    #Coordinate of the Initial condition with a red dot.
    xi = [0.99]
    yi = [0.99]
    zi = [0.99]
    ax.plot(xi, yi ,zi, 'bo', color = 'red')

    #Drawing the trajectory
    ax.plot(x, y, z, linestyle = '--', color = 'blue', linewidth=0.75)

    #Drawing the trajectory
    ax.plot(w, wi, er,color = 'black', linewidth=0.75)

    #Labeling
    ax.set_xlabel(r'x')
    ax.set_ylabel(r"y")
    ax.zaxis.set_rotate_label(False)    #it allows to rotate the label!
    ax.set_zlabel(r"z", rotation=0)
    ax.tick_params(labelsize=10)    #resizing the numbers

    #Setting the box size
    ax.set_xlim(0.0, 1.0)
    ax.set_ylim(0.0, 1.0)
    ax.set_zlim(0.0, 1.0)


    plt.savefig('output/trajectory' + N_str1 + '.png', bbox_inches='tight')

    n+=1

plt.show()